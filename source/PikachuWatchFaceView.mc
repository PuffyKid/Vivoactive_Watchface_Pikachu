using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Lang as Lang;
using Toybox.Application as App;
using Toybox.Time.Gregorian as Greg;
using Toybox.Time as Time;
using Toybox.ActivityMonitor as AM;

/*
 * A blinking pikachu watch face. It looks alot like pikachu in pokemon yellow.
 * @author Matt Litchfield
 */
class PikachuWatchFaceView extends Ui.WatchFace {

  var pikachuFace0;
  var pikachuFace1;
  var pikachuFace2;
  var pikachuFace3;
  var pokeball;
  var greatball;
  var ultraball;
  var masterball;
  var counter=0;
  
    function initialize() {
        WatchFace.initialize();
    }

    //! Load your resources here
    function onLayout(dc) {
    	System.println("Loading images");
        setLayout(Rez.Layouts.WatchFace(dc));
        pikachuFace0= Ui.loadResource(Rez.Drawables.pikachu0);
   	 	pikachuFace1= Ui.loadResource(Rez.Drawables.pikachu1);
   	 	pikachuFace2= Ui.loadResource(Rez.Drawables.pikachu2);
   	 	pikachuFace3= Ui.loadResource(Rez.Drawables.pikachu3);
   	 	pokeball= Ui.loadResource(Rez.Drawables.pokeball);
   	 	greatball= Ui.loadResource(Rez.Drawables.greatball);
   	 	ultraball= Ui.loadResource(Rez.Drawables.ultraball);
   	 	masterball= Ui.loadResource(Rez.Drawables.masterball);   	 	
	}

    //! Called when this View is brought to the foreground. Restore
    //! the state of this View and prepare it to be shown. This includes
    //! loading resources into memory.
    function onShow() {

    }

    //! Update the view
    function onUpdate(dc) {

    	dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_BLACK);

    	dc.clear();
        // Get and show the current time

        var clockTime = Sys.getClockTime();

        var timeString = Lang.format("$1$$2$", [clockTime.hour.format("%02d"), clockTime.min.format("%02d")]);
        var view = View.findDrawableById("TimeLabel");
        view.setText(timeString);
        setCalender(); 
        setStepCounter();                

        // Call the parent onUpdate function to redraw the layout
		View.onUpdate(dc);
		
		var battery=Sys.getSystemStats().battery;

		dc.drawBitmap(34, 7, (battery>60)?((battery>90)?masterball:ultraball):((battery>30)?greatball:pokeball));
		dc.drawBitmap(138, 7, (battery>45)?((battery>75)?masterball:ultraball):((battery>15)?greatball:pokeball));
		
		if(counter == 0){
		 	dc.drawBitmap(45, 45, pikachuFace0);
		}else if(counter == 1){
			dc.drawBitmap(45, 45, pikachuFace1);
		}else if(counter == 2){
			dc.drawBitmap(45, 45, pikachuFace2);
		}else{
			dc.drawBitmap(45, 45, pikachuFace3);
			counter=-1;
		}
		counter = counter+1;
		System.println("Battery:"+ battery+",Steps:"+AM.getInfo().steps+",Pikachu Face:"+counter);


    }
	function setCalender(){		
		var time=Greg.info(Time.now(),Time.FORMAT_MEDIUM);
		var view = View.findDrawableById("DateLabel1");
        view.setText(time.month.substring(0,1));
        view = View.findDrawableById("DateLabel2");
        view.setText(time.month.substring(1,2));
		view = View.findDrawableById("DateLabel3");
        view.setText(time.month.substring(2,3));
		view = View.findDrawableById("DateLabel4");
        view.setText(" ");
		view = View.findDrawableById("DateLabel5");
        view.setText((time.day/10).toString());
		view = View.findDrawableById("DateLabel6");
        view.setText((time.day%10).toString());       

	}
	
	function setStepCounter(){
		var step=AM.getInfo().steps;
		var view = View.findDrawableById("StepLabel5");
	    view.setText((step%10).toString());
	    step=step/10;
	    view = View.findDrawableById("StepLabel4");
	    view.setText((step%10).toString());
	    step=step/10;
		view = View.findDrawableById("StepLabel3");
	    view.setText((step%10).toString());
	    step=step/10;
		view = View.findDrawableById("StepLabel2");
	    view.setText((step%10).toString());
	    step=step/10;
		view = View.findDrawableById("StepLabel1");
	    view.setText((step%10).toString());         
	}
    //! Called when this View is removed from the screen. Save the
    //! state of this View here. This includes freeing resources from
    //! memory.
    function onHide() {
    }

    //! The user has just looked at their watch. Timers and animations may be started here.
    function onExitSleep() {
    }

    //! Terminate any active timers and prepare for slow updates.
    function onEnterSleep() {
    }	
}

